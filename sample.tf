# Get Resources from a Resource Group
data "azurerm_resources" "example" {
  resource_group_name = "example-resources"
}

# Get Resources with specific Tags
data "azurerm_resources" "example" {
  resource_group_name = "example-resources"

  required_tags = {
    environment = "production"
    role        = "webserver"
  }
}

# Get resources by type, create spoke vNet peerings
data "azurerm_resources" "spokes" {
  type = "Microsoft.Network/virtualNetworks"

  required_tags = {
    environment = "production"
    role        = "spokeNetwork"
  }
}

resource "azurerm_virtual_network_peering" "spoke_peers" {
  count = length(data.azurerm_resources.spokes.resources)

  name                      = "hub2${data.azurerm_resources.spokes.resources[count.index].name}"
  resource_group_name       = azurerm_resource_group.hub.name
  virtual_network_name      = azurerm_virtual_network.hub.name
  remote_virtual_network_id = data.azurerm_resources.spokes.resources[count.index].id
}
